/*
商城VUEX-actions
*/
export default{
    // 定义方法  保存用户信息
        //context  必须存在.暴露上下文关系
        //userName  参数  用户名
    saveUserName(context,username){
        context.commit('saveUserName',username);
    },
    saveCartCount(context,count){
        context.commit('saveCartCount',count);
    },
}