import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'
//引入文件

//把该文件导入VUE实例
Vue.use(Vuex);
  //定义state 保存 通用的数据 ,设置默认值
const state={
  username:'',//登录用户名称
  cartCount:0
};
// 导出
export default new Vuex.Store({
  state,
  mutations,  //原样式是 state:{.. } 现在我们单独拆出   把state{}   mutations{} 的内容在mutation内书写
  actions
})
