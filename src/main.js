import Vue from 'vue'
// 引入插件
import axios from "axios"
import VueAxios from 'vue-axios'
import App from './App.vue'
import router from "./router"
import VueLazyLoad from 'vue-lazyload'
import store from './store'
import VueCookie from 'vue-cookie'
import env from "./env"
import {Message} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

//下载mockjs  集成Mock API :加载MOCK server,存放到项目中去,变相作为连接池,拦截 链接 进行数据调试
const mock = false;
if (mock) {
    // 加载它 
    //这里使用了 require  为什么不用 import因为引入之后,会一直执行 mockApi拦截,那样的你数据永远被限制在 本地 不能发出
    // 而require 加载  只有打开时才会被执行 就是上边的mock  true false  
    // require('./mock/api')
}
// 根据前端的跨域方式做调整 /a/b : /api/a/b =>发送时/a/b
axios.defaults.baseURL = '/api'; //设置默认 网址
axios.defaults.timeout = 5000; //设置超时的时间




// 根据环境变量获取不同的请求地址
// axios.defaults.baseURL=env.baseURL;


//接口错误拦截 
axios.interceptors.response.use(function (response) {
    // 拿到我们需要的响应数据
    let res = response.data;
    //获取当前页面的路径名称
    let path = location.pathname;
    // 当响应请求的状态码是0 时,说明请求成功,那就把请求成功,并返回的数据 return 出去
    if (res.status == 0) {
        return res.data;

        // 否则 如果 当前响应回来的响应状态码 是10(我们自定义的)
        // 说明登陆请求发送失败 ,具体我们会根据响应的响应状态码,来确定出现的情况
        // 例 10是用户未登陆 12 商品不存在 13 用户支付失败 都是根据请求的状态码 来判断当前内容
    } else if (res.status == 10) {
        if (path != '/index') {
            // 跳转到登陆页面
         
             window.location.href = '/login';  
                         
        }
        // window.location.href = '/login'; 
        return Promise.reject(res);  
    } else {
        //除此以外进行,错误显示
        
        // this.$message.warning(res.msg);
        // alert(res.msg)
        // console.log(res.msg)
        Message.warning(res.msg)
        // 抛出异常
        return Promise.reject(res);
    }
},(err)=>{
    let res=err.response;
    Message.error(res.data.Message);
    return Promise.reject(err);
}
);

// 加载插件  把插件,加载到vue 实例中去
Vue.use(VueAxios, axios);
Vue.use(VueCookie);
// Vue.use(Message); //这里加入原型后,不用再次加入实例了.否则会出现bug 每次进入自动执行message

Vue.use(VueLazyLoad, {
    loading: '/imgs/loading-svg/loading-bars.svg'
})
// 加载到原型上
Vue.prototype.$message=Message;
Vue.config.productionTip = false

new Vue({
    // 这部操作相当于  router :router
    router,
    store,
    render: h => h(App)
}).$mount('#app')