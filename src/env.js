//声明默认请求地址 变量
let baseURL;
//process.env  获取nodejs进程中的环境变量
//然后根据变量的不同,选择不同的url 地址 
switch (process.env.NODE_ENV) {
    case 'development':
        baseURL = 'htt://dev-mall-pre.springboot.cn/api';
        break;
    case 'test':
        baseURL = 'htt://test-mall-pre.springboot.cn/api';
        break;
    case 'prod':
        baseURL = 'htt://mall-pre.springboot.cn/api';
        break;
    default: baseURL = 'htt://mall-pre.springboot.cn/api';
        break;
}

// 根据环境的不同,来输出不同的URL地址
// 最后导出 基准路径
export default {
    baseURL
}