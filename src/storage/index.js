//在storage的index,封装Storage,,
//我们选择封装SEssionStorage,他是储存在内存当中,当浏览器关闭时,内容被清除
const STORAGE_KEY='mall';
// 导出
export default{
    // 存储storage值() 
    /*
    1.其中key指 实例化的storag  如我们编辑的mall  是键值对 中的键
    2.value 是当前所存储的实例,但是,因为默认存储是JSON格式,所以传实参时,我们需要转成字符串 就是键值对中的值 value
    3.module_name 是键值对中某条属性的  一条的具体的属性名, 如user下的 user:{"userName"} 从而获得相应的值 类型user.userName 
    */
    setItem(key,value,module_name){
        if(module_name){
            // 首先判断是否有单个storage 属性名实参传入
            //然后 调用getitemAPI  获取该实参的具体数据 如 userName:  得到数值123
            let val=this.getItem(module_name);
            //通过强行赋值的方式,添加给 
            val[key]=value;
            // 再次调用自定义setItem (键 key,value值)
            this.setItem(module_name,val);
        }else{
             // 首先获取全部的storage 数值
        let val=this.getStorage();
        // 再在 要存的数值,以强行添加的形式 储存到 val(也就是全部storage值中)
        val[key]=value;
        // 存完之后,我们重新写入到storage中,具体方法是window.sessionStorage 

        //调用当前函数,把实例化的内容传进去,
        
        // module_name指 指storage中某一个实例的属性名,从而获取具体某一个属性值
        window.sessionStorage.setItem(STORAGE_KEY,JSON.stringify(val));
        }    
    },

    // 获取单个storage值
    // 既然是获取单个值,就必须要有形参,你要获取哪个值?
    // 首先,获取全部的Storage,然后 然后用形参的KEY确定获取具体哪一个参数
    getItem(key,module_name){
        // 最后把获取到的单个storage 返回出去
        //然后你想获取,key内部的数值,用递归的方法,传递自己内部的属性名, 调用自己的方法
           if(module_name){
            //如果有该参数时,优先获取该参数,然后在获取该元素
               let val=this.getItem(module_name);
               if(val) return val[key];
           } 
       return this.getStorage()[key]
    },



    // 获取全部的storage值
    getStorage(){
        // API 获取全部的storage值       获取mall的变量
        // Json.pase()  转成JSon 字符串形式
        // retuan 最后返回出去 Json转换后的数据出去
      return JSON.parse(window.sessionStorage.getItem(STORAGE_KEY) || '{}');
    },
    // 清除所有d storage值
    //key  哪个模块  module_name   你要删除模块下的哪一个具体的属性
    clear(key,module_name){
        /*
            1.获取全部的storage属性值
        */
       let val=this.getStorage();
       // 2.如果你有模块内具体属性,就先获取该模块内容
       if(module_name){
        //  如果需要删除的内容不存在没救返回出去    
            if(!val[module_name])return;

        //   3. 然后删除掉 该 x 模块下的xx属性
           delete val[module_name][key];
       }else{
        //  4. 否则 如果没有该属性,就直接删除整个 x 模块
           delete val[key]
       }
       //   5.然后把删除属性后的内容,写到 storage中
       window.sessionStorage.setItem(STORAGE_KEY,JSON.stringify(val));
    }
}